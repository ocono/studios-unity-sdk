# Changelog

All notable changes to SDK will be documented in this file.

## [Unreleased]
   Upcoming features...

## [0.3.1] - 2020-02
   Fixed exception when cStringCopy is used by other SDK's.

## [0.3.0] - 2020-01
   New feature parameter storage. And interface allowing retrieving values as configured in Weq studios backend system. 
   Could be used for an AB testing.
   
   From this version valid access token must be provided, otherwise all endpoints will return 401 response code.
   The access token can be provided in Weq Analytics context menu from Unity editor.
   
### Added 
   New method: `WeqAnalytics.ParameterStore.RegisterParameter("PARAM_NAME", "defaultValue", "description")` to retrieve dynamic value.

## [0.2.0] - 2019-12
  API changes
  
### Added
  - New methods to track user interface, monetization, resource events. 

## [0.1.1] - 2019-11
  Initial SDK release
    
### Added
  - Basic high level API for tracking events
  - Simple configuration panel in UNITY editor
  - iOS SDK integration
  - Android SDK integration 
