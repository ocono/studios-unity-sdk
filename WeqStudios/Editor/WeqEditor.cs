using UnityEditor;
using UnityEngine;
using WeqStudios.SDK;

namespace WeqStudios.Editor {
    [CustomEditor(typeof(WeqSettings))]
    public class Editor: UnityEditor.Editor {

        void OnEnable() {
        }

        public override void OnInspectorGUI() {
            EditorGUILayout.BeginVertical();

            EditorGUILayout.LabelField("Tracker", EditorStyles.boldLabel);
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(serializedObject.FindProperty("appToken"), new GUIContent("Application token"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("gameId"), new GUIContent("Game id"), true);
            
            EditorGUILayout.EndVertical();
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}