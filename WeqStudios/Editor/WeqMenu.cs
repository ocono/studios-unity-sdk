using UnityEditor;

namespace WeqStudios.Editor {
       
    public class Menu {
        [MenuItem ("Window/WeQAnalytics", false, 1)]
        public static void MenuSettings() {
            SettingsWindow.ShowWindow();
        }
    }
}