using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using WeqStudios.SDK;

namespace WeqStudios.Editor {
    public class SettingsWindow : EditorWindow {

        private UnityEditor.Editor _editor;
        public static void ShowWindow() {
            var window = GetWindow();
            window.titleContent = new GUIContent("WeQ Studios");
            window.Show();
        }

        private static EditorWindow GetWindow() {
            Type inspectorType = Type.GetType("UnityEditor.InspectorWindow,UnityEditor.dll");
            var window = GetWindow<SettingsWindow>(new Type[] {inspectorType});
            window.titleContent = new GUIContent("WeQ Studios", "");

            return window;
        }

        private void OnGUI() {
            if (_editor == null) {
                _editor = UnityEditor.Editor.CreateEditor(WeqSettings.Instance);
            }

            _editor.DrawHeader();
            _editor.OnInspectorGUI();
        }
    }
}