using Debug = UnityEngine.Debug;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System;
using System.Text.RegularExpressions;

namespace WeqStudios.Editor {
    public class XcodePostProcess {
        //Callback order must be between 40 - 50, because some SDK's uses google "unity-jar-resolver" which overwrites PodFile.
        [PostProcessBuildAttribute(42)]
        public static void OnPostProcessBuild(BuildTarget buildTarget, string buildPath) {
            if (buildTarget != BuildTarget.iOS) {
                return;
            }

            SetEnvironmentVariables();
            Install(buildPath);
            ConfigureXcodeBuild(buildPath);
        }

        private static void SetEnvironmentVariables() {
            Environment.SetEnvironmentVariable("LANG", "en_US.UTF-8");
            Environment.SetEnvironmentVariable("PATH", "/usr/local/bin/:" + Environment.GetEnvironmentVariable("PATH"), EnvironmentVariableTarget.Process);
        }

        private static void Install(string buildPath) {
            var currentDirectory = Directory.GetCurrentDirectory();

            ConfigurePodFile(buildPath, currentDirectory);

            Directory.SetCurrentDirectory(buildPath);
            var log = ExecuteCommand("pod", "install");
            Debug.Log(log);
            Directory.SetCurrentDirectory(currentDirectory);
        }

        private static void ConfigureXcodeBuild(String buildPath) {
            var projectPath = PBXProject.GetPBXProjectPath(buildPath);
            var project = new PBXProject();
            project.ReadFromFile(projectPath);

            var targetGuid = project.TargetGuidByName(PBXProject.GetUnityTargetName());

            project.SetBuildProperty(targetGuid, "CLANG_ENABLE_MODULES", "YES");
            project.SetBuildProperty(targetGuid, "OTHER_CPLUSPLUSFLAGS", "$(inherited) -fcxx-modules");
            project.SetBuildProperty(targetGuid, "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES", "YES");

            project.WriteToFile(projectPath);
        }

        private static void ConfigurePodFile(string buildPath, string currentDirectory) {
            var podFileSourcePath = Path.Combine(currentDirectory, "Assets", WeqConfig.PluginRootPath, "SDK", "Library", "iOS", "Podfile");
            var podFileDestinationPath = Path.Combine(buildPath, "Podfile");
            if (!File.Exists(podFileDestinationPath)) {
                File.Copy(podFileSourcePath, podFileDestinationPath);
            }
            else {
                var updated = File.ReadAllLines(podFileDestinationPath).Where(line => !line.Contains("WeqSdk"));
                var currentPodContents = string.Join(" \n", updated).Replace("end", "");
                var weqPodContents = File.ReadAllLines(podFileSourcePath);
                var weqDependencies = weqPodContents.SkipWhile(x => x != "target 'Unity-iPhone' do")
                    .Skip(1) //Skip "target 'Unity-iPhone' do" 
                    .TakeWhile(x => x != "end")
                    .ToArray();

                currentPodContents += string.Join(" \n", weqDependencies) + "\nend";

                //WeqSdk uses Swift and it only works with iOS framework.
                if (!currentPodContents.Contains("use_frameworks")) {
                    currentPodContents += "\nuse_frameworks!\n";
                }

                File.WriteAllText(podFileDestinationPath, currentPodContents);
            }
        }

        private static string ExecuteCommand(string command, string arguments) {
            var processStart = new ProcessStartInfo {
                FileName = command,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                StandardOutputEncoding = System.Text.Encoding.UTF8,
                StandardErrorEncoding = System.Text.Encoding.UTF8
            };
            if (arguments != null) {
                processStart.Arguments = arguments;
            }

            processStart.RedirectStandardError = true;

            var process = Process.Start(processStart);
            if (process == null) return "";

            var output = process.StandardOutput.ReadToEnd();
            var error = process.StandardError.ReadToEnd();
            process.WaitForExit();
            if (!string.IsNullOrEmpty(error)) {
                Debug.LogError(error);
            }

            process.Close();
            return output;
        }
    }
}