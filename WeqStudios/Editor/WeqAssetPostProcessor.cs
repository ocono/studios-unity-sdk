using System.IO;
using UnityEditor;
using UnityEngine;
using WeqStudios.SDK;

namespace WeqStudios.Editor {
    public class WeqAssetPostProcessor : AssetPostprocessor {

        static WeqAssetPostProcessor() {
            var settings = WeqSettings.LoadSettingsAsset();
            if (settings) {
                return;
            }
            CreateSettings();
        }

        private static void CreateSettings() {
            var resourcePath = Application.dataPath + "/" + WeqConfig.PluginRootPath + "Resources";
            if (!Directory.Exists(resourcePath)) {
                Directory.CreateDirectory(resourcePath);
            }

            const string path = "Assets/" + WeqConfig.PluginRootPath + "Resources/WeqSettings.asset";
            var assets = WeqSettings.Instance;
            AssetDatabase.CreateAsset(assets, path);
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }
    }
}