namespace WeqStudios.SDK {
    public class EventType {
        public const string GamePlay = "Gameplay";
        public const string Progression = "Progression";
        public const string Monetization = "Monetization";
        public const string Resource = "Resource";
        public const string UserInterface = "UserInterface";
    }
}