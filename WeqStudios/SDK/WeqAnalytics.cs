﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using WeqStudios.SDK.Model;
using static WeqStudios.SDK.Utils.MiniJSON;
using System.Collections.Concurrent;
using System.Reflection;

namespace WeqStudios.SDK {
    public class WeqAnalytics : MonoBehaviour {
        private static bool _initialized = false;

        public static void Initialize() {
            if (_initialized) {
                return;
            }

            WeqDeviceProxy.Configure(WeqSettings.Instance.AppToken, WeqSettings.Instance.GameId);
            _initialized = true;
        }

        public static void Debug(bool enabled) {
            WeqDeviceProxy.DebugLog(enabled);
        }

        public static void DisableTracking() {
            WeqDeviceProxy.DisableTracking();
        }

        public static void EnableTracking() {
            WeqDeviceProxy.EnableTracking();
        }

        public static void Event(string category, string name, List<CustomTag> tags) {
            WeqDeviceProxy.Event(category, name, SerializeTags(tags), null, null, null, null, null);
        }

        public static void Event(string category, string name, List<CustomTag> tags, CustomValue value1) {
            WeqDeviceProxy.Event(category, name, SerializeTags(tags), Serialize(value1.ToDict()), null, null, null, null);
        }

        public static void Event(string category, string name, List<CustomTag> tags, CustomValue value1, CustomValue value2) {
            WeqDeviceProxy.Event(category, name, SerializeTags(tags), Serialize(value1.ToDict()), Serialize(value2.ToDict()), null, null, null);
        }

        public static void Event(string category, string name, List<CustomTag> tags, CustomValue value1, CustomValue value2, CustomValue value3) {
            WeqDeviceProxy.Event(category, name, SerializeTags(tags), Serialize(value1.ToDict()), Serialize(value2.ToDict()), Serialize(value3.ToDict()), null, null);
        }

        public static void Event(string category, string name, List<CustomTag> tags, CustomValue value1, CustomValue value2, CustomValue value3, CustomValue value4, CustomValue value5) {
            WeqDeviceProxy.Event(category, name, SerializeTags(tags), Serialize(value1.ToDict()), Serialize(value2.ToDict()), Serialize(value3.ToDict()), Serialize(value4.ToDict()), Serialize(value5.ToDict()));
        }

        private static string SerializeTags(List<CustomTag> tags) {
            var tagsStringArray = tags.Select(tag => Serialize(tag.ToDict())).ToList();
            return Serialize(tagsStringArray);
        }

        public static class ParameterStore {
            public static Parameter<T> RegisterParameter<T>(string name, T defaultValue, string description) {
                var parameter = new Parameter<T>(name, defaultValue);
                WeqDeviceProxy.RegisterParameter(name, parameter.ParameterType, defaultValue.ToString(), description);
                return parameter;
            }
        }

        public static class CasualGame {
            public static class GamePlay {
                public static void Action(string actionName, List<CustomTag> tags) {
                    Event(EventType.GamePlay, actionName, tags);
                }
            }

            public static class UserInterface {
                public static void Action(string actionName, List<CustomTag> tags) {
                    Event(EventType.UserInterface, actionName, tags);
                }
            }

            public static class Progression {
                public static void StartLevel(string levelName, List<CustomTag> tags) {
                    tags.Add(new CustomTag("__ProgressionAction", "Start"));
                    Event(EventType.Progression, levelName, tags);
                }

                public static void CompleteLevel(string levelName, List<CustomTag> tags) {
                    tags.Add(new CustomTag("__ProgressionAction", "Complete"));
                    Event(EventType.Progression, levelName, tags);
                }

                public static void FailLevel(string levelName, List<CustomTag> tags) {
                    tags.Add(new CustomTag("__ProgressionAction", "Fail"));
                    Event(EventType.Progression, levelName, tags);
                }

                public static void AbandonLevel(string levelName, List<CustomTag> tags) {
                    tags.Add(new CustomTag("__ProgressionAction", "Abandon"));
                    Event(EventType.Progression, levelName, tags);
                }
            }

            public static class Resource {
                private static readonly ConcurrentDictionary<string, double> ResourceTotals = new ConcurrentDictionary<string, double>();

                public static void IncrementBy(double amount, string resourceName, string cause, List<CustomTag> tags) {
                    tags.Add(new CustomTag("__ResourceCause", cause));

                    ResourceTotals.AddOrUpdate(resourceName, amount, (key, existingAmount) => existingAmount + amount);

                    var amountValue = new CustomValue(amount.ToString(CultureInfo.InvariantCulture));
                    var totalValue = new CustomValue(ResourceTotals.GetOrAdd(resourceName, 0).ToString(CultureInfo.InvariantCulture));

                    Event(EventType.Resource, resourceName, tags, amountValue, totalValue);
                }

                public static void DecrementBy(double amount, string resourceName, string cause, List<CustomTag> tags) {
                    IncrementBy(-amount, resourceName, cause, tags);
                }

                public static void Exchange(string exchangeName, List<ResourceItem> paid, List<ResourceItem> received, List<CustomTag> tags) {
                    foreach (var paidItem in paid) {
                        DecrementBy(paidItem.GetAmount(), paidItem.GetResourceName(), exchangeName, tags);
                    }

                    foreach (var paidItem in received) {
                        IncrementBy(paidItem.GetAmount(), paidItem.GetResourceName(), exchangeName, tags);
                    }
                }

                public static void Exchange(string exchangeName, double paidAmount, string paidResourceName, double receivedAmount, string receivedResourceName, List<CustomTag> tags) {
                    Exchange(
                        exchangeName,
                        new List<ResourceItem>() {new ResourceItem(paidAmount, paidResourceName)},
                        new List<ResourceItem>() {new ResourceItem(receivedAmount, receivedResourceName)},
                        tags
                    );
                }
            }

            public static class Monetization {
                public static void Purchase(string itemName, double hardCurrencyAmount, string currency, double resourceAmount, string resourceName, List<CustomTag> tags) {
                    var amountValue = new CustomValue(hardCurrencyAmount.ToString(CultureInfo.InvariantCulture), null);
                    Event(EventType.Monetization, itemName, tags, amountValue);

                    tags.Add(new CustomTag("__MonetizationItem", itemName));
                    tags.Add(new CustomTag("__Currency", currency));

                    Resource.IncrementBy(hardCurrencyAmount, "__Money", itemName, tags);
                    Resource.Exchange("__MonetizationExchange", hardCurrencyAmount, "__Money", resourceAmount, resourceName, tags);
                }

                public static void Purchase(string itemName, double hardCurrencyAmount, string currency, string cause, List<CustomTag> tags) {
                    var amountValue = new CustomValue(hardCurrencyAmount.ToString(CultureInfo.InvariantCulture), null);
                    Event(EventType.Monetization, itemName, tags, amountValue);

                    tags.Add(new CustomTag("__MonetizationItem", itemName));
                    tags.Add(new CustomTag("__Currency", currency));

                    Resource.IncrementBy(hardCurrencyAmount, "__Money", itemName, tags);
                    Resource.DecrementBy(hardCurrencyAmount, "__Money", cause, tags);
                }

                public static void AddView(string placement, double resourceAmount, string resourceName, List<CustomTag> tags) {
                    tags.Add(new CustomTag("__Placement", placement));

                    Event(EventType.Monetization, "__AdView", tags, new CustomValue("1"));
                    Resource.IncrementBy(1, "__Money", "__AdView", tags);
                    Resource.Exchange("__AdViewExchange", 1, "__AdView", resourceAmount, resourceName, tags);
                }

                public static void AddView(string placement, string cause, List<CustomTag> tags) {
                    tags.Add(new CustomTag("__Placement", placement));

                    Event(EventType.Monetization, "__AdView", tags, new CustomValue("1"));
                    Resource.IncrementBy(1, "__AdView", null, tags);
                    Resource.DecrementBy(1, "__AdView", cause, tags);
                }
            }
        }
    }
}