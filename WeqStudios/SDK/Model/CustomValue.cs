using System.Collections.Generic;

namespace WeqStudios.SDK.Model {
    public class CustomValue {
        
        private string value;
        private string uom;

        public CustomValue(string value, string uom) {
            this.value = value;
            this.uom = uom;
        }
        
        public CustomValue(string value) {
            this.value = value;
        }
        
        public Dictionary<string, string> ToDict() {
            return new Dictionary<string, string> {{"value", value}, {"uom", uom}};
        }
    }
}