using System;
using System.Collections.Generic;
using UnityEngine;

namespace WeqStudios.SDK.Model {
    public class Parameter<T> {
        private readonly string _name;
        private readonly T _defaultValue;
        private T _value;
        private readonly List<string> _allowedTypes = new List<string>() {"string", "boolean", "double", "int64" ,"long"};

        public Parameter(string name, T defaultValue) {
            ParameterType = typeof(T).Name.ToLower();
            AdjustType(); 
            if (!_allowedTypes.Contains(ParameterType)) {
                throw new Exception($"Type '{ParameterType}' is not allowed, use one of ({string.Join(", ", _allowedTypes)})");
            }
            
            _name = name;
            _defaultValue = defaultValue;
        }

        private void AdjustType() {
            ParameterType = ParameterType.Equals("int64") ? "long" : ParameterType;
        }

        public string ParameterType { get; set; }

        public T Get() {
            var value = WeqDeviceProxy.GetParameter(_name);
            if (!string.IsNullOrEmpty(value)) {
                return (T) Convert.ChangeType(value, typeof(T));
            }

            return _defaultValue;
        }
    }
}