using System.Collections.Generic;

namespace WeqStudios.SDK.Model {
    public class CustomTag {
        
        private string name;
        private string value;

        public CustomTag(string name, string value) {
            this.name = name;
            this.value = value;
        }

        public Dictionary<string, string> ToDict() {
            return new Dictionary<string, string> {{"name", name}, {"value", value}};
        }
    }
}