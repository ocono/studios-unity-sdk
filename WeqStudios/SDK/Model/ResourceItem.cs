namespace WeqStudios.SDK {
    public class ResourceItem {

        private readonly double amount;
        private readonly string resourceName;
        
        public ResourceItem(double amount, string resourceName) {
            this.amount = amount;
            this.resourceName = resourceName;
        }

        public double GetAmount() {
            return this.amount;
        }

        public string GetResourceName() {
            return this.resourceName;
        }
    }
}