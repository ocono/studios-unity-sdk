#import "WeqAnalytics.h"
@import WeqSdk;

#pragma mark - C interface
extern "C" {
    void _configure(const char *appToken, const char *gameId) {
        NSString *appTokenString = appToken != NULL ? [NSString stringWithUTF8String:appToken] : nil;
        NSString *gameIdString = gameId != NULL ? [NSString stringWithUTF8String:gameId] : nil;
    
        [WeqAnalytics configureWithAppToken:appTokenString gameId:gameIdString];
    }

    void _event(const char *category, const char *name, const char *tags, const char *value1, const char *value2, const char *value3, const char *value4, const char *value5) {
         NSString *tagsString = tags != NULL ? [NSString stringWithUTF8String:tags] : nil;
         NSArray *tagsArray = nil;
         if (tagsString) {
             tagsArray = [NSJSONSerialization JSONObjectWithData:[tagsString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
         }
         NSString *categoryString = category != NULL ? [NSString stringWithUTF8String:category] : nil;
         NSString *nameString = name != NULL ? [NSString stringWithUTF8String:name] : nil;
         NSString *value1String = value1 != NULL ? [NSString stringWithUTF8String:value1] : nil;
         NSString *value2String = value2 != NULL ? [NSString stringWithUTF8String:value2] : nil;
         NSString *value3String = value3 != NULL ? [NSString stringWithUTF8String:value3] : nil;
         NSString *value4String = value4 != NULL ? [NSString stringWithUTF8String:value4] : nil;
         NSString *value5String = value5 != NULL ? [NSString stringWithUTF8String:value5] : nil;
            
        [WeqAnalytics eventWithCategory:categoryString name:nameString tags:tagsArray value1:value1String value2:value2String value3:value3String value4:value4String value5:value5String];
    }
    
    void _debug(BOOL enable) {
        [WeqAnalytics debugWithEnable:enable];
    }
    
    void _disableTracking() {
        [WeqAnalytics disableTracking];
    }
    
    void _enableTracking() {
        [WeqAnalytics enableTracking];
    }
    
    char* weq_cStringCopy(const char* string) {
        if (string == NULL)
            return NULL;
    
        char* res = (char*)malloc(strlen(string) + 1);
        strcpy(res, string);
    
        return res;
    }
    
    void _registerParameter(const char *name, const char *type, const char *defaultValue, const char *description) {
        NSString *nameString = name != NULL ? [NSString stringWithUTF8String:name] : nil;
        NSString *typeString = type != NULL ? [NSString stringWithUTF8String:type] : nil;
        NSString *defaultValueString = type != NULL ? [NSString stringWithUTF8String:defaultValue] : nil;
        NSString *descriptionString = type != NULL ? [NSString stringWithUTF8String:description] : nil;
    
       [WeqAnalytics registerParameterWithName:nameString type:typeString defaultValue:defaultValueString description:descriptionString];
    }
    
    char* _getParameterWithName(const char *name) {
        NSString *nameString = name != NULL ? [NSString stringWithUTF8String:name] : nil;
        NSString *result = [WeqAnalytics getParameterWithName:nameString];
        
        return weq_cStringCopy([result UTF8String]);
    }
}