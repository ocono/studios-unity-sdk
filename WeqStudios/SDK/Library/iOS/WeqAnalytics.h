#import <Foundation/Foundation.h>

@interface WeqAnalytics : NSObject
+ (void)configureWithAppToken:(NSString * _Nonnull)appToken gameId:(NSString * _Nonnull)gameId;
+ (void)eventWithCategory:(NSString * _Nonnull)category name:(NSString * _Nonnull)name tags:(NSArray<NSString *> * _Nonnull)tags value1:(NSString * _Nonnull)value1 value2:(NSString * _Nonnull)value2 value3:(NSString * _Nonnull)value3 value4:(NSString * _Nonnull)value4 value5:(NSString * _Nonnull)value5;
+ (void)disableTracking;
+ (void)enableTracking;
+ (void)debugWithEnable:(BOOL)enable;
+ (void)registerParameterWithName:(NSString * _Nonnull)name type:(NSString * _Nonnull)type defaultValue:(NSString * _Nonnull)defaultValue description:(NSString * _Nonnull)description;
+ (NSString * _Nullable)getParameterWithName:(NSString * _Nonnull)name;
@end