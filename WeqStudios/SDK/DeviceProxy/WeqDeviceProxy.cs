using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace WeqStudios.SDK {
    public partial class WeqDeviceProxy {
#if (UNITY_EDITOR || (!UNITY_IOS && !UNITY_ANDROID))
        public static void Configure(string appToken, string gameId) {
            Debug.Log($"Configure({appToken}, {gameId})");
        }

        public static void Event(string category, string name, string tags, string event1, string event2, string event3, string event4, string event5) {
            Debug.Log($"Event({category}, {name}, {tags}, {event1}, {event2}, {event3}, {event4}, {event5})");
        }

        public static void DebugLog(bool enabled) {
            Debug.Log($"Debug({enabled})");
        }

        public static void DisableTracking() {
            Debug.Log("DisableTracking()");
        }
        
        public static void EnableTracking() {
            Debug.Log("EnableTracking()");
        }
        
        public static void RegisterParameter(string name, string type, string defaultValue, string description) {
            Debug.Log($"RegisterParameter({name}, {type}, {defaultValue}, {description})");
        }
        
        public static string GetParameter(string name) {
            Debug.Log($"GetParameter({name})");
            return null;
        }
#endif
    }
}