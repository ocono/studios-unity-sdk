using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.Analytics;
using WeqStudios.SDK.Utils;

namespace WeqStudios.SDK {
    public partial class WeqDeviceProxy {
#if (UNITY_ANDROID) && !(UNITY_EDITOR)
            private static readonly AndroidJavaClass Analytics = new AndroidJavaClass("com.weq.studios.sdk.WeqAnalytics");
        
            public static void Configure(string appToken, string gameId) {
                 AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                 AndroidJavaObject activity = jc.GetStatic<AndroidJavaObject>("currentActivity");

                 Analytics.CallStatic("configure", activity, appToken, gameId);        
            }
        
            public static void Event(string category, string name, string tags, string value1, string value2, string value3, string value4, string value5) {
                 IList<object> list = WeqStudios.SDK.Utils.MiniJSON.Deserialize(tags) as IList<object>;
                 ArrayList tagsArray = new ArrayList();
                 foreach(object entry in list){
                    tagsArray.Add(entry);
                 }
                 Analytics.CallStatic("event", category, name, tagsArray.ToArray(typeof(string)), value1, value2, value3, value4, value5);        
            }
        
            public static void DebugLog(bool enabled) {
                Analytics.CallStatic("debug", enabled);   
            }
        
            public static void DisableTracking() {
                Analytics.CallStatic("disableTracking");
            }  

            public static void EnableTracking() {
                Analytics.CallStatic("enableTracking");
            } 
        
            public static void RegisterParameter(string name, string type, string defaultValue, string description) {
                Analytics.CallStatic("registerParameter", name, type, defaultValue, description);   
            }
        
            public static string GetParameter(string name) {
                return Analytics.CallStatic<string>("getParameter", name);  
            }
#endif
    }

}