using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace WeqStudios.SDK {
    public partial class WeqDeviceProxy {
        #region Declare external C interface

#if (UNITY_IOS) && (!UNITY_EDITOR)
                [DllImport("__Internal")]
                private static extern void _configure(string appToken, string gameId);
                
                [DllImport("__Internal")]
                private static extern void _event(string category, string name, string tags, string value1, string value2, string value3, string value4, string value5);
        
                [DllImport("__Internal")]
                private static extern void _debug(bool enabled);
        
                [DllImport("__Internal")]            
                private static extern void _disableTracking();
        
                [DllImport("__Internal")]            
                private static extern void _enableTracking();
        
                [DllImport("__Internal")]            
                private static extern void _registerParameter(string name, string type, string defaultValue, string description);
        
                [DllImport("__Internal")]   
                [return: MarshalAs(UnmanagedType.LPStr)]
                private static extern string _getParameterWithName(string name);
#endif

        #endregion

        #region Wrapped methods and properties

#if (UNITY_IOS) && (!UNITY_EDITOR)
                public static void Configure(string appToken, string gameId) {
                     _configure(appToken, gameId);    
                }
                
                public static void Event(string category, string name, string tags, string value1, string value2, string value3, string value4, string value5) {
                     _event(category, name, tags, value1, value2, value3, value4, value5);
                }
        
                public static void DebugLog(bool enable) {
                    _debug(enable);         
                }
        
                public static void DisableTracking() {
                    _disableTracking();
                }  

                public static void EnableTracking() {
                    _enableTracking();
                }   

                public static void RegisterParameter(string name, string type, string defaultValue, string description) {
                     _registerParameter(name, type, defaultValue, description);
                }
        
                public static string GetParameter(string name) {
                     return _getParameterWithName(name);
                }
#endif
        #endregion
    }
}