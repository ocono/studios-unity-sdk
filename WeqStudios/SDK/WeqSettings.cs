using UnityEngine;
using System.IO;
using UnityEditor;

namespace WeqStudios.SDK {
    public class WeqSettings : ScriptableObject {
        private static WeqSettings _weqSettingInstance;

        [SerializeField] private string appToken;
        [SerializeField] private string gameId;

        public string AppToken {
            get => appToken;
            set => appToken = value;
        }

        public string GameId {
            get => gameId;
            set => gameId = value;
        }

        public static WeqSettings Instance {
            get {
                if (_weqSettingInstance != null) {
                    return _weqSettingInstance;
                }

                _weqSettingInstance = LoadSettingsAsset();
                if (_weqSettingInstance != null) {
                    return _weqSettingInstance;
                }

                return CreateInstance<WeqSettings>();
            }
        }

        public static WeqSettings LoadSettingsAsset() {
            return Resources.Load("WeqSettings") as WeqSettings;
        }
    }
}