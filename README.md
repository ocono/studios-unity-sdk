# WeQ Studios SDK Unity Plugin

WeQ Studios Analytics Plugin integrates with unity game engine for games build for iOS and Android.
It provides API for the game developer to invoke tracking events. Which could be used to analyse interactions, collect user device data for additional statistics.

Current release is in Beta stage and we are still testing/exploring SDK use cases, fixing bugs.

## Install

Download plugin from "https://bitbucket.org/ocono/studios-unity-sdk/downloads/?tab=tags"

Unzip and copy `WeqStudios` contents to Unity assets directory. `{ProjectRoot}/Assets/WeqStudios`.

Click `Window->WeQAnalytics`, the form will appear in inspector window. Enter "Application token" and "Game id".

Initialize SDK before game starts by calling `WeqAnalytics.Initialize()`. Enable debug output with `WeqAnalytics.Debug(true)`.

Select `File->Build Settings` and pick platform "Android" or "iOS", select "Development Build" then click "Build".

To build and run project select `File->Build And Run`.

If everything works correctly you can see SDK debug output in XCode console for iOS or Logcat for Android.

If you want to place plugin to other directory, change  `WeqConfig.PluginRootPath` accordingly.

#### iOS

SDK source code: https://bitbucket.org/ocono/studios-ios-sdk.git

iOS requires Cocoapods installed on machine Unity is runnning.

#### Android

SDK source code: https://bitbucket.org/ocono/studios-android-sdk.git

To collect all the data SDK needs few permissions.

 * "android.permission.ACCESS_NETWORK_STATE" - for collecting network related information
 * "android.permission.INTERNET" - for sending data to our servers
 * "com.google.android.providers.gsf.permission.READ_GSERVICES" - to read google advertiser id (GSFID)

There is two android AAR library version "fat" and "minimal"

 * "studios-android-sdk-{version}-fat.aar" - with all libraries
 * "studios-android-sdk-{version}-minimal.aar" - without dependencies defined, only library code, in case you have problems with conflicting dependencies you may want to use this.

Pick one, then delete other.

For minimal android library you need to include additional dependencies to Unity generated Gradle file. 
You can read more about it at: https://docs.unity3d.com/Manual/android-gradle-overview.html

Add this to ``Assets/Plugins/Android/mainTemplate.gradle``
```
dependencies {
   // ... other dependencies
   implementation "com.google.code.gson:gson:2.8.6"
   implementation "com.github.nisrulz:easydeviceinfo:2.4.1"
   implementation "com.github.nisrulz:easydeviceinfo-base:2.4.1"
   implementation "com.github.nisrulz:easydeviceinfo-ads:2.4.1"
   implementation "com.github.nisrulz:easydeviceinfo-common:2.4.1"
   implementation "com.google.android.gms:play-services-basement:17.1.1"
   implementation 'com.google.android.gms:play-services-ads-identifier:17.0.0'
 }
```

## Tracking API

There are number of API methods which you can use.
```
 Each API method can have tags argument set, which can be use to add additional metadata/context to tracked event.  

 For example structure of tags:
 var tags = new List<WeqStudios.SDK.Model.CustomTag>();
 tags.Add(new WeqStudios.SDK.Model.CustomTag("custom-tag1", "tag-value1"));
 tags.Add(new WeqStudios.SDK.Model.CustomTag("custom-tag2", "tag-value2"));

 To inititialize tracker before sending events
 WeqAnalytics.Initialize()

 To track custom gameplay actions.
 WeqAnalytics.CasualGame.Gameplay.Action("my-action", tags)

 To track custom interface actions.
 WeqAnalytics.CasualGame.UserInterface.Action("my-action", tags)

 // Progression

 To track beginning of level
 WeqAnalytics.CasualGame.Progression.StartLevel("level1", tags)

 To track completion of level
 WeqAnalytics.CasualGame.Progression.CompleteLevel("level1", tags)

 To track failure of level completion
 WeqAnalytics.CasualGame.Progression.FailLevel("level1", tags)

 To track abandoned level
 WeqAnalytics.CasualGame.Progression.AbandonLevel("level1", tags)

 // Resource

 To track resource increments
 WeqAnalytics.CasualGame.Resource.IncrementBy(10, "diamond", "playerLevelUp", tags)

 To track custom resource decrements
 WeqAnalytics.CasualGame.Resource.DecrementBy(1, "diamond", "levelFailed", tags)

 To track exchange multiple resources with another

 var paid = new List<WeqStudios.SDK.Model.ResourceItem>();
 paid.Add(new WeqStudios.SDK.Model.ResourceItem("gold", "1"));
 paid.Add(new WeqStudios.SDK.Model.ResourceItem("silver", "5"));
 
 var received = new List<WeqStudios.SDK.Model.ResourceItem>();
 received.Add(new WeqStudios.SDK.Model.ResourceItem("diamond", "1"));

 WeqAnalytics.CasualGame.Resource.Exchange("levelUp", paid, received, tags)

 To track exchange of single resource
 WeqAnalytics.CasualGame.Resource.Exchange("500Points", 50, "copper", 1, "silver" tags)

 //Monetization

 To track purchase of recource by real currency
 WeqAnalytics.CasualGame.Monetization.Purchase("diamond_pack", 2.99, "EUR", 5, "diamond", tags)

 To track custom purchase
 WeqAnalytics.CasualGame.Monetization.Purchase("subcriber", 1, "EUR", "Monthly subscription", tags)

 To track the view of an Add, with bonus resources
 WeqAnalytics.CasualGame.Monetization.AddView("bottom", 20, "copper", tags)
 
 To track the view of an Add, without bonus resources
 WeqAnalytics.CasualGame.Monetization.AddView("bottom", "Add no reward", tags)

 To register value dynamic parameter with default value, the second parameter is generic you can use ("long", "string", "boolean", "double")
 var parameter = WeqAnalytics.ParameterStore.RegisterParameter("PARAMETER_NAME", "DEFAULT_VALUE", "The parameter");
 /*
    Retrieve value  by calling parameter.Get().
    The value is usefull to make AB testing as it can be resolved to different value based on predefined parameters. The calculation happens in WeQ backend system.
 */

 To create custom event, with any custom configuration
 WeqAnalytics.Event(
  "monetization", //The event category
  "Refund", //The event name
  tags, //The additional tags for a specific custom event
  new WeqStudios.SDK.Model.CustomValue("A"), //The event custom value1
  new WeqStudios.SDK.Model.CustomValue("B"), //The event custom value2
  new WeqStudios.SDK.Model.CustomValue("C"), //The event custom value3
  new WeqStudios.SDK.Model.CustomValue("D"), //The event custom value4
  new WeqStudios.SDK.Model.CustomValue("E")  //The event custom value5
)

 To enable debug login from SDK, default is disabled
 WeqAnalytics.DebugLog(true|false)

 To disable tracking, default is tracking
 WeqAnalytics.DisableTracking()

 To enable tracking, default is tracking
 WeqAnalytics.EnableTracking()

```
